//////////////////////////////////////////////////////////////////////////////////////////
// INICIO KERNEL GENERIC

//4.15 INICIO opção Remove ou instalar O kernel
$('#chKernel415').click(function(){
  if($('#chKernel415').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel415('install')
      },
      close: function(){
        getkernel415();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel415('remove')
      },
      close: function(){
        getkernel415();
      }
    })
  }
});
function kernel415(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.15.18-041518 linux-headers-4.15.18-041518-generic linux-image-4.15.18-041518-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel415();
  });
}
function getkernel415(){
  shell.exec('find /boot -name initrd.img-4.15*', (a, b, c) => {
    var versao = b.substring(17, 39);
    if (versao == '4.15.18-041518-generic')
    $('#chKernel415').prop('checked', true);
    else
    $('#chKernel415').prop('checked', false)
  });
}
getkernel415()
//4.15 FIM ação de remover ou adicionar kernel

//4.14 INICIO opção Remove ou instalar O kernel
$('#chKernel414').click(function(){
  if($('#chKernel414').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel414('install')
      },
      close: function(){
        getkernel414();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel414('remove')
      },
      close: function(){
        getkernel414();
      }
    })
  }
});
function kernel414(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-image-4.14.35-041435-generic linux-headers-4.14.35-041435 linux-headers-4.14.35-041435-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel414();
  });
}
function getkernel414(){
  shell.exec('find /boot -name initrd.img-4.14*', (a, b, c) => {
    var versao = b.substring(17, 39);
    if (versao == '4.14.35-041435-generic')
    $('#chKernel414').prop('checked', true);
    else
    $('#chKernel414').prop('checked', false)
  });
}
getkernel414()
//4.14 FIM ação de remover ou adicionar kernel

//4.13 INICIO opção Remove ou instalar O kernel
$('#chKernel413').click(function(){
  if($('#chKernel413').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel413('install')
      },
      close: function(){
        getkernel413();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel413('remove')
      },
      close: function(){
        getkernel413();
      }
    })
  }
});
function kernel413(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.13.16-041316 linux-headers-4.13.16-041316-generic linux-image-4.13.16-041316-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel413();
  });
}
function getkernel413(){
  shell.exec('find /boot -name initrd.img-4.13*', (a, b, c) => {
    var versao = b.substring(17, 39);
    if (versao == '4.13.16-041316-generic')
    $('#chKernel413').prop('checked', true);
    else
    $('#chKernel413').prop('checked', false)
  });
}
getkernel413()
//4.13 FIM ação de remover ou adicionar kernel

//4.12 INICIO opção Remove ou instalar O kernel
$('#chKernel412').click(function(){
  if($('#chKernel412').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel412('install')
      },
      close: function(){
        getkernel412();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel412('remove')
      },
      close: function(){
        getkernel412();
      }
    })
  }
});
function kernel412(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.12.14-041214 linux-headers-4.12.14-041214-generic linux-image-4.12.14-041214-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel412();
  });
}
function getkernel412(){
  shell.exec('find /boot -name initrd.img-4.12*', (a, b, c) => {
    var versao = b.substring(17, 39);
    if (versao == '4.12.14-041214-generic')
    $('#chKernel412').prop('checked', true);
    else
    $('#chKernel412').prop('checked', false)
  });
}
getkernel412()
//4.12 FIM ação de remover ou adicionar kernel

//4.11 INICIO opção Remove ou instalar O kernel
$('#chKernel411').click(function(){
  if($('#chKernel411').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel411('install')
      },
      close: function(){
        getkernel411();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel411('remove')
      },
      close: function(){
        getkernel411();
      }
    })
  }
});
function kernel411(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.11.12-041112 linux-headers-4.11.12-041112-generic linux-image-4.11.12-041112-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel411();
  });
}
function getkernel411(){
  shell.exec('find /boot -name initrd.img-4.11*', (a, b, c) => {
    var versao = b.substring(17, 39);
    if (versao == '4.11.12-041112-generic')
    $('#chKernel411').prop('checked', true);
    else
    $('#chKernel411').prop('checked', false)
  });
}
getkernel411()
//4.11 FIM ação de remover ou adicionar kernel

//4.10 INICIO opção Remove ou instalar O kernel
$('#chKernel410').click(function(){
  if($('#chKernel410').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel410('install')
      },
      close: function(){
        getkernel410();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel410('remove')
      },
      close: function(){
        getkernel410();
      }
    })
  }
});
function kernel410(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.10.1-041001 linux-headers-4.10.1-041001-generic linux-image-4.10.1-041001-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel410();
  });
}
function getkernel410(){
  shell.exec('find /boot -name initrd.img-4.10*', (a, b, c) => {
    var versao = b.substring(17, 38);
    if (versao == '4.10.1-041001-generic')
    $('#chKernel410').prop('checked', true);
    else
    $('#chKernel410').prop('checked', false)
  });
}
getkernel410()
//4.10 FIM ação de remover ou adicionar kernel

//4.9 INICIO opção Remove ou instalar O kernel
$('#chKernel49').click(function(){
  if($('#chKernel49').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel49('install')
      },
      close: function(){
        getkernel49();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel49('remove')
      },
      close: function(){
        getkernel49();
      }
    })
  }
});
function kernel49(acao){
  $('#pnPreLoad').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.9.13-040913 linux-headers-4.9.13-040913-generic linux-image-4.9.13-040913-generic && update-grub2', (a, b, c) => {
    $('#pnPreLoad').css('display', 'none');
    getkernel49();
  });
}
function getkernel49(){
  shell.exec('find /boot -name initrd.img-4.9*', (a, b, c) => {
    var versao = b.substring(17, 20);
    if (versao == '4.9')
    $('#chKernel49').prop('checked', true);
    else
    $('#chKernel49').prop('checked', false)
  });
}
getkernel49()
//4.9 FIM ação de remover ou adicionar kernel

//FIM KERNEL GENERIC


// INICIO KERNEL LowLatence
//4.15ll INICIO opção Remove ou instalar O kernel
$('#chKernel415ll').click(function(){
  if($('#chKernel415ll').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel415ll('install')
      },
      close: function(){
        getkernel415ll();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel415ll('remove')
      },
      close: function(){
        getkernel415ll();
      }
    })
  }
});
function kernel415ll(acao){
  $('#pnPreLoadll').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.15.18-041518-lowlatency linux-image-4.15.18-041518-lowlatency && update-grub2', (a, b, c) => {
    $('#pnPreLoadll').css('display', 'none');
    getkernel415ll();
  });
}
function getkernel415ll(){
  shell.exec('find /boot -name initrd.img-4.15.18-041518*', (a, b, c) => {
    var versao = b.substring(17, 42);
    if (versao == '4.15.18-041518-lowlatency')
    $('#chKernel415ll').prop('checked', true);
    else
    $('#chKernel415ll').prop('checked', false)
  });
}
getkernel415ll()
//4.15ll FIM ação de remover ou adicionar kernel

//4.14ll INICIO opção Remove ou instalar O kernel
$('#chKernel414ll').click(function(){
  if($('#chKernel414ll').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel414ll('install')
      },
      close: function(){
        getkernel414ll();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel414ll('remove')
      },
      close: function(){
        getkernel414ll();
      }
    })
  }
});
function kernel414ll(acao){
  $('#pnPreLoadll').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.14.35-041435-lowlatency linux-image-4.14.35-041435-lowlatency && update-grub2', (a, b, c) => {
    $('#pnPreLoadll').css('display', 'none');
    getkernel414ll();
  });
}
function getkernel414ll(){
  shell.exec('find /boot -name initrd.img-4.14.35-041435*', (a, b, c) => {
    var versao = b.substring(17, 42);
    if (versao == '4.14.35-041435-lowlatency')
    $('#chKernel414ll').prop('checked', true);
    else
    $('#chKernel414ll').prop('checked', false)
  });
}
getkernel414ll()
//4.14ll FIM ação de remover ou adicionar kernel

//4.13ll INICIO opção Remove ou instalar O kernel
$('#chKernel413ll').click(function(){
  if($('#chKernel413ll').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel413ll('install')
      },
      close: function(){
        getkernel413ll();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel413ll('remove')
      },
      close: function(){
        getkernel413ll();
      }
    })
  }
});
function kernel413ll(acao){
  $('#pnPreLoadll').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.13.16-041316-lowlatency linux-image-4.13.16-041316-lowlatency && update-grub2', (a, b, c) => {
    $('#pnPreLoadll').css('display', 'none');
    getkernel413ll();
  });
}
function getkernel413ll(){
  shell.exec('find /boot -name initrd.img-4.13.16-041316*', (a, b, c) => {
    var versao = b.substring(17, 42);
    if (versao == '4.13.16-041316-lowlatency')
    $('#chKernel413ll').prop('checked', true);
    else
    $('#chKernel413ll').prop('checked', false)
  });
}
getkernel413ll()
//4.13ll FIM ação de remover ou adicionar kernel

//4.12ll INICIO opção Remove ou instalar O kernel
$('#chKernel412ll').click(function(){
  if($('#chKernel412ll').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel412ll('install')
      },
      close: function(){
        getkernel412ll();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel412ll('remove')
      },
      close: function(){
        getkernel412ll();
      }
    })
  }
});
function kernel412ll(acao){
  $('#pnPreLoadll').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.12.14-041214-lowlatency linux-image-4.12.14-041214-lowlatency && update-grub2', (a, b, c) => {
    $('#pnPreLoadll').css('display', 'none');
    getkernel412ll();
  });
}
function getkernel412ll(){
  shell.exec('find /boot -name initrd.img-4.12.14-041214*', (a, b, c) => {
    var versao = b.substring(17, 42);
    if (versao == '4.12.14-041214-lowlatency')
    $('#chKernel412ll').prop('checked', true);
    else
    $('#chKernel412ll').prop('checked', false)
  });
}
getkernel412ll()
//4.12ll FIM ação de remover ou adicionar kernel

//4.11ll INICIO opção Remove ou instalar O kernel
$('#chKernel411ll').click(function(){
  if($('#chKernel411ll').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel411ll('install')
      },
      close: function(){
        getkernel411ll();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel411ll('remove')
      },
      close: function(){
        getkernel411ll();
      }
    })
  }
});
function kernel411ll(acao){
  $('#pnPreLoadll').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.11.12-041112-lowlatency linux-image-4.11.12-041112-lowlatency && update-grub2', (a, b, c) => {
    $('#pnPreLoadll').css('display', 'none');
    getkernel411ll();
  });
}
function getkernel411ll(){
  shell.exec('find /boot -name initrd.img-4.11.12-041112*', (a, b, c) => {
    var versao = b.substring(17, 42);
    if (versao == '4.11.12-041112-lowlatency')
    $('#chKernel411ll').prop('checked', true);
    else
    $('#chKernel411ll').prop('checked', false)
  });
}
getkernel411ll()
//4.11ll FIM ação de remover ou adicionar kernel

//4.10ll INICIO opção Remove ou instalar O kernel
$('#chKernel410ll').click(function(){
  if($('#chKernel410ll').prop('checked')==true){
    confirma({
      msg:locate.confirmaInstall,
      title:locate.tituloMofo,
      call: function(){
        kernel410ll('install')
      },
      close: function(){
        getkernel410ll();
      }
    })
  }
  else{
    confirma({
      msg:locate.confirmaRemove,
      title:locate.tituloMofo,
      call: function(){
        kernel410ll('remove')
      },
      close: function(){
        getkernel410ll();
      }
    })
  }
});
function kernel410ll(acao){
  $('#pnPreLoadll').css('display', 'flex');
  shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.10.17-041017 linux-headers-4.10.17-041017-lowlatency linux-image-4.10.17-041017-lowlatency && update-grub2', (a, b, c) => {
    $('#pnPreLoadll').css('display', 'none');
    getkernel410ll();
  });
}
function getkernel410ll(){
  shell.exec('find /boot -name initrd.img-4.10.17-041017*', (a, b, c) => {
    var versao = b.substring(17, 42);
    if (versao == '4.10.17-041017-lowlatency')
    $('#chKernel410ll').prop('checked', true);
    else
    $('#chKernel410ll').prop('checked', false)
  });
}
getkernel410ll()
//4.10ll FIM ação de remover ou adicionar kernel
//FIM KERNEL LowLatence

//INICO Kernel Liquorix
//4.12li INICIO opção Remove ou instalar O kernel
$('#chKernel412li').click(function(){
if($('#chKernel412li').prop('checked')==true){
  confirma({
    msg:locate.confirmaInstall,
    title:locate.tituloMofo,
    call: function(){
      kernel412li('install')
    },
    close: function(){
      getkernel412li();
    }
  })
}
else{
  confirma({
    msg:locate.confirmaRemove,
    title:locate.tituloMofo,
    call: function(){
      kernel412li('remove')
    },
    close: function(){
      getkernel412li();
    }
  })
}
});
function kernel412li(acao){
$('#pnPreLoadli').css('display', 'flex');
shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.12.0-14.3-liquorix-amd64 linux-image-4.12.0-14.3-liquorix-amd64 && update-grub2', (a, b, c) => {
  $('#pnPreLoadli').css('display', 'none');
  getkernel412li();
});
}
function getkernel412li(){
shell.exec('find /boot -name initrd.img-4.12.0-14.3*', (a, b, c) => {
  var versao = b.substring(17, 37);
  if (versao == '4.12.0-14.3-liquorix')
  $('#chKernel412li').prop('checked', true);
  else
  $('#chKernel412li').prop('checked', false)
});
}
getkernel412li()
//4.12li FIM ação de remover ou adicionar kernel

//4.11li INICIO opção Remove ou instalar O kernel
$('#chKernel411li').click(function(){
if($('#chKernel411li').prop('checked')==true){
  confirma({
    msg:locate.confirmaInstall,
    title:locate.tituloMofo,
    call: function(){
      kernel411li('install')
    },
    close: function(){
      getkernel411li();
    }
  })
}
else{
  confirma({
    msg:locate.confirmaRemove,
    title:locate.tituloMofo,
    call: function(){
      kernel411li('remove')
    },
    close: function(){
      getkernel411li();
    }
  })
}
});
function kernel411li(acao){
$('#pnPreLoadli').css('display', 'flex');
shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.11.0-12.1-liquorix-amd64 linux-image-4.11.0-12.1-liquorix-amd64 && update-grub2', (a, b, c) => {
  $('#pnPreLoadli').css('display', 'none');
  getkernel411li();
});
}
function getkernel411li(){
shell.exec('find /boot -name initrd.img-4.11.0-12.1*', (a, b, c) => {
  var versao = b.substring(17, 37);
  if (versao == '4.11.0-12.1-liquorix')
  $('#chKernel411li').prop('checked', true);
  else
  $('#chKernel411li').prop('checked', false)
});
}
getkernel411li()
//4.11li FIM ação de remover ou adicionar kernel

//4.10li INICIO opção Remove ou instalar O kernel
$('#chKernel410li').click(function(){
if($('#chKernel410li').prop('checked')==true){
  confirma({
    msg:locate.confirmaInstall,
    title:locate.tituloMofo,
    call: function(){
      kernel410li('install')
    },
    close: function(){
      getkernel410li();
    }
  })
}
else{
  confirma({
    msg:locate.confirmaRemove,
    title:locate.tituloMofo,
    call: function(){
      kernel410li('remove')
    },
    close: function(){
      getkernel410li();
    }
  })
}
});
function kernel410li(acao){
$('#pnPreLoadli').css('display', 'flex');
shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.10.0-16.1-liquorix-amd64 linux-image-4.10.0-16.1-liquorix-amd64 && update-grub2', (a, b, c) => {
  $('#pnPreLoadli').css('display', 'none');
  getkernel410li();
});
}
function getkernel410li(){
shell.exec('find /boot -name initrd.img-4.10.0-16.1*', (a, b, c) => {
  var versao = b.substring(17, 37);
  if (versao == '4.10.0-16.1-liquorix')
  $('#chKernel410li').prop('checked', true);
  else
  $('#chKernel410li').prop('checked', false)
});
}
getkernel410li()
//4.10li FIM ação de remover ou adicionar kernel

//4.9li INICIO opção Remove ou instalar O kernel
$('#chKernel49li').click(function(){
if($('#chKernel49li').prop('checked')==true){
  confirma({
    msg:locate.confirmaInstall,
    title:locate.tituloMofo,
    call: function(){
      kernel49li('install')
    },
    close: function(){
      getkernel49li();
    }
  })
}
else{
  confirma({
    msg:locate.confirmaRemove,
    title:locate.tituloMofo,
    call: function(){
      kernel49li('remove')
    },
    close: function(){
      getkernel49li();
    }
  })
}
});
function kernel49li(acao){
$('#pnPreLoadli').css('display', 'flex');
shell.exec(' apt-get '+acao+' --yes --force-yes linux-headers-4.9.0-49.1-liquorix-amd64 linux-image-4.9.0-49.1-liquorix-amd64 && update-grub2', (a, b, c) => {
  $('#pnPreLoadli').css('display', 'none');
  getkernel49li();
});
}
function getkernel49li(){
shell.exec('find /boot -name initrd.img-4.9.0-49.1*', (a, b, c) => {
  var versao = b.substring(17, 36);
  if (versao == '4.9.0-49.1-liquorix')
  $('#chKernel49li').prop('checked', true);
  else
  $('#chKernel49li').prop('checked', false)
});
}
getkernel49li()
//4.9li FIM ação de remover ou adicionar kernel

//FIM KERNEL Liquorix


//INICIO Apresenta Kernel atual GENERIC///////////////////////////////////////////////////////////////////////////////////////////
shell.exec('uname -r', (a, b, c) => {
  $('#kernelatual').html(b);
});
//FIM Apresenta Kernel atual GENERIC//////////////////////////////////////////////////////////////////////////////////////////////
//INICIO Apresenta Kernel atual LOW///////////////////////////////////////////////////////////////////////////////////////////
shell.exec('uname -r', (a, b, c) => {
  $('#kernelatuallow').html(b);
});
//FIM Apresenta Kernel atual LOW//////////////////////////////////////////////////////////////////////////////////////////////
//INICIO Apresenta Kernel atual Liquorix///////////////////////////////////////////////////////////////////////////////////////////
shell.exec('uname -r', (a, b, c) => {
  $('#kernelatualli').html(b);
});
//FIM Apresenta Kernel atual Liquorix//////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function () {
  $('.tabs').tabs();
});
