const electron = require('electron')
const {app, BrowserWindow} = electron;

app.on('ready', () => {

	let win = new BrowserWindow({
		width : 650,
		height: 600,
		icon: __dirname + '/images/icon.png',
		show:false
	})

	win.loadURL('file://' + __dirname + '/index.html')
	win.on('ready-to-show',()=>{
	win.show()
	})
win.on('closed',()=>win=null)

})

	app.on('window-all-closed', ()=>{
	app.quit()
	})
