# DKI DuZeru Kernel Install

Uma vez estava visualizando images de aplicativos para plataforma Linux, uma forma de se inspirar em desenvolver algo, foi quando eu encontrei uma imagem do sistema operacional Manjaro com um instalador de Kernel. que bela ideia. Para minha tristeza, este vinha vinculado a interface utilizada. Mas ta aí uma boa aplicação, para utilizadores Debian e Ubuntu, esta aplicação ainda não existe, foi daí que iniciou-se as atividades para desenvolver a apliação de instalador de Kernel.

As primeiras vieram através arquivos de Shell Script.
A segunda versão ganhou uma melhoria com a interface gráfica de WebKit de Python e HTML.
Quase chegando na terceira versão, ganhou outra reescrita, agora com scripts simples em Zenity.

Definitivamente junto a versão 3.0 do DuZeru, com a ajuda do colaborador Francisco Alves, foi novamente reescrita utilizando o Electron e as tecnologias adjacentes.